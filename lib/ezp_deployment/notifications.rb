# Add Mac OS X Desktop notifications through the terminal-notifier gem
require 'capistrano'

class CapNotifications
  def self.load_into(cap_config)
    cap_config.load do
      notification_enabled = false

      before 'deploy', 'notification:info'
      after 'deploy', 'notification:finished'

      begin
        gem 'terminal-notifier'
        notification_enabled = true
      rescue Gem::LoadError =>e
      end

      set(:terminal_app) do
        if fetch(:has_iterm, false) then 'com.googlecode.iterm2' else 'com.apple.Terminal' end
      end

      namespace :notification do
        task :info do
          if notification_enabled then
            puts ' ** Desktop notifications enabled'.green
          else
            puts ' ** Desktop notifications disbaled'.green
          end
        end

        desc "Send a desktop notification when deploy finishes"
        task :finished do
          on_rollback {
            if notification_enabled then
            run_locally "terminal-notifier -activate #{terminal_app} -group #{terminal_app} -title 'Failure' -message 'Capistrano deployment to #{stage} for #{application} failed'"
            end
          }
          if notification_enabled then
            run_locally "terminal-notifier -activate #{terminal_app} -group #{terminal_app} -title 'Success' -message 'Capistrano finished deployment to #{stage} for #{application}'"
          end
        end
      end
    end
  end
end

if Capistrano::Configuration.instance
    CapNotifications.load_into(Capistrano::Configuration.instance)
end

# This is a generic deployment recipie for deploying eZ Publish 5.3.
require 'capistrano'

class CapeZDeploy53
  def self.load_into(cap_config)
    cap_config.load do
      after "deploy:update", "deploy:cleanup"

      after "deploy:finalize_update", "deploy:create_symlinks"
      after "deploy:create_symlinks", "deploy:composer_install"
      after "deploy:composer_install", "deploy:generate_autoloads"
      after "deploy:composer_install", "deploy:fix_permissions"

      namespace :deploy do
        desc "This is here to override the original :restart"
        task :restart, :roles => :app do
          # do nothing but overide the default
        end

        desc "Show the date and time of last deployment"
        task :last_deployment, :roles => :app do
          run "echo 'Last deployment was at:' && date -r #{current_release}/REVISION +'%d-%m-%Y %T'"
        end

        desc "Performance last set of deployment tasks"
        task :finalize_update, :roles => :app do
          run "chmod -R g+w #{latest_release}" if fetch(:group_writable, true)

        end

        desc "Create symlinks to shared data such as config files and var"
        task :create_symlinks, :roles => :app do
          run "cd #{current_release} && ./symsync.sh #{environment}"

          #Create folders if they are not present
          run "if [ ! -d #{current_release}/ezpublish/cache ]; then mkdir #{current_release}/ezpublish/cache; fi;"
          run "if [ ! -d #{current_release}/ezpublish/sessions ]; then mkdir #{current_release}/ezpublish/sessions; fi;"

          # Symlink in static assets for eZ Legacy
          run "if [ ! -d #{current_release}/ezpublish_legacy/var/ezflow_site ]; then mkdir -p #{current_release}/ezpublish_legacy/var/ezflow_site; fi;"
          run "ln -s #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/ezflow_site/storage #{current_release}/ezpublish_legacy/var/ezflow_site/storage"

          run "if [ -d #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/storage ]; then ln -s #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/storage #{current_release}/ezpublish_legacy/var/storage; fi;"

          # Sitemaps folders
          run "if [ ! -d #{current_release}/ezpublish_legacy/var/ezflow_site/cache/public ]; then mkdir -p #{current_release}/ezpublish_legacy/var/ezflow_site/cache/public; fi;"
          run "if [ -d #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/ezflow_sites/cache/public/sitemaps ]; then ln -s #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/ezflow_site/cache/public/sitemaps #{current_release}/ezpublish_legacy/var/ezflow_site/cache/public/sitemaps; fi;"

          run "if [ -d #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/cache/public ]; then mkdir -p #{current_release}/ezpublish_legacy/var/cache/public; fi;"
          run "if [ -d #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/cache/public/sitemaps ]; then ln -s #{deploy_to}#{shared_dir}/assets/ezpublish_legacy/var/cache/public/sitemaps #{current_release}/ezpublish_legacy/var/cache/public/sitemaps; fi;"
        end

        desc "Composer install"
        task :composer_install, :roles => :app do
          run "cd #{current_release} && SYMFONY_ENV=#{env} #{php_bin} composer.phar install --prefer-dist --no-dev --optimize-autoloader --no-interaction"
        end

        desc "Generate eZ Publish Legacy autoloads"
        task :generate_autoloads, :roles => :app do
          run "cd #{current_release}/ezpublish_legacy && #{php_bin} bin/php/ezpgenerateautoloads.php"
          run "cd #{current_release}/ezpublish_legacy && #{php_bin} bin/php/ezpgenerateautoloads.php -o"
        end

        desc "Fix permissions for some of the files and folders"
        task :fix_permissions, :roles => :app do
          run "cd #{current_release} && find {ezpublish/{cache,sessions,logs,config},ezpublish_legacy/{design,settings,var},web} -type d | xargs chmod -R 777"
          run "cd #{current_release} && find {ezpublish/{cache,sessions,logs,config},ezpublish_legacy/{design,settings,var},web} -type f | xargs chmod -R 666"
        end

        desc "Copy storage folder from deploy user's home to shared directory"
        task :copy_var, :roles => :app do
          # copy the var folder
          run "cp -R /home/#{user}/var #{deploy_to}/#{shared_dir}"
          # reset permissions
          run "chmod -R 777 #{deploy_to}/#{shared_dir}/var"
        end
      end

      # Overriding capifony tasks that are not relevant for eZ 5
      namespace :symfony do
        namespace :bootstrap do
          task :build, :roles => :app, :except => { :no_release => true } do
            puts " ** Skipping capifony task".yellow
          end
        end

        namespace :cache do
          task :warmup, :roles => :app do
            puts " ** Skipping capifony task".yellow
          end
        end

        namespace :project do
          task :clear_controllers do
            puts " ** Skipping capifony task".yellow
          end
        end
      end
    end
  end
end

if Capistrano::Configuration.instance
  CapeZDeploy53.load_into(Capistrano::Configuration.instance)
end

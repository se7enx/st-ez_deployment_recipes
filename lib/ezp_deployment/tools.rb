# Holds all Capistrano related tasks that can be shared across all sites
# Anything you put in this file must be generic enough to be used across all
# sites.
require 'capistrano'

class CapeZSyncAssets
  def self.load_into(cap_config)
    cap_config.load do
      namespace :sync_to_local do
        desc "Synchronizes remote database & assets to local development environment and clears cache"
        task :default, :roles => :master do
          db and assets and clear_cache
        end

        desc "Synchronizes remote database to local development environment"
        task :db, :roles => :master do
          filename = "database.#{application}.#{stage}.#{Time.now.strftime '%Y-%m-%d_%H:%M:%S'}.sql.gz"
          run "mysqldump -u #{mysql_user} --password='#{mysql_pwd}' -h#{mysql_server} --single-transaction --quick --extended-insert #{mysql_db} | gzip -c > /home/deploy/#{filename}" do |channel, stream, data|
            puts data
          end
          download "/home/deploy/#{filename}", "/tmp/#{filename}"
          system "gunzip -c /tmp/#{filename} | mysql -u #{dev_mysql_user} --password='#{dev_mysql_pwd}' -h#{dev_mysql_server} #{dev_mysql_db}; rm -f /tmp/#{filename}"
          run "rm /home/deploy/#{filename}"
          logger.important "sync database from the '#{stage}' environment to dev finished"
        end

        desc "Synchronizes remote assets to local development environment"
        task :assets do
          server = roles[:master].servers.first.host
          system "rsync -azv --no-perms --no-times --chmod=Dg+rwxs,Fg+rw --delete -e ssh #{user}@#{server}:#{asset_path} #{dev_asset_path}"
          logger.important "sync assets from the '#{stage}' environment  to local finished"
        end

        desc "Clears the dev cache (local)"
        task :clear_cache do
          system "php bin/php/ezcache.php --clear-all --purge"
        end

        desc "Backup local database & assets dir"
        task :local_backup do
          filename = "database.#{application}.sql.gz"
          system "cp -Rp #{dev_asset_path}/var #{dev_asset_path}/var-backup"
          system "mysqldump --single-transaction --quick -u #{dev_mysql_user} --password='#{dev_mysql_pwd}' -h#{dev_mysql_server} #{dev_mysql_db} | bzip2 -c -9 > #{dev_asset_path}#{filename}"
        end
      end
    end
  end
end


if Capistrano::Configuration.instance
  CapeZSyncAssets.load_into(Capistrano::Configuration.instance)
end

# This file contains a set of tasks to put up and remove an eZ Publish
# holdingpage. `cap holdingpage:on` creates a symlink called 'maintanance.html' of an
# HTML file specified by the variable #{holdingpage_html} in the root of the
# site. `cap holdingpage:off` removes the same symlink.
#
# The holdingpage is automatically turned on as part of the deployment process.
class CapeZHoldingpage
  def self.load_into(cap_config)
    cap_config.load do
      before "deploy:composer_install", "holdingpage:internal_on"
      after "deploy", "holdingpage:reminder"

      namespace :holdingpage do
        desc "Creates a holding page. Only for interal use only"
        task :internal_on, :roles => :app do
          run "if [ -e #{current_release}/web ]; then cd #{current_release}/web && ln -sf #{holdingpage_html} maintenance.html; fi"
          run "if [ -e #{deploy_to}#{current_dir}/web ]; then cd #{deploy_to}#{current_dir}/web && ln -sf #{holdingpage_html} maintenance.html; fi"
        end

        desc "Creates a holding page"
        task :on, :roles => :app do
          run "cd #{current_path}/web && ln -sf #{holdingpage_html} maintenance.html"
        end

        desc "Removes all holding pages"
        task :off, :roles => :app do
          run "cd #{current_path}/web && rm -f maintenance.html"
          run "cd #{releases_path}/ && rm -f ./{*}/web/maintenance.html"
        end

        desc "Reminder to turn off the holding page"
        task :reminder do
          puts "\nREMEMBER TO TURN OFF THE HOLDING PAGE WHEN YOU HAVE FINISHED TESTING THE SITE\n\n"
        end
      end
    end
  end
end


if Capistrano::Configuration.instance
  CapeZHoldingpage.load_into(Capistrano::Configuration.instance)
end

# This file contains a set of tasks to put up and remove an eZ Publish
# holdingage. `cap holdingpage:on` creates a symlink called 'maintanance.html' of an
# HTML file specified by the variable #{holdingpage_html} in the root of the
# site. `cap holdingpage:off` removes the same symlink.
#
# The holdingpage is automatically turned on as part of the deployment process.
class CapeZHoldingpage
  def self.load_into(cap_config)
    cap_config.load do
      before "deploy:clear_cache", "holdingpage:internal_on"
      after "deploy", "holdingpage:reminder"

      namespace :holdingpage do
        desc "Creates a holding page. Only for interal use only"
        task :internal_on, :roles => :web do
          run "if [ -e #{current_release} ]; then cd #{current_release} && ln -sf #{holdingpage_html} maintenance.html; fi"
          run "if [ -e #{deploy_to}/#{current_dir}/ ]; then cd #{deploy_to}/#{current_dir}/ && ln -sf #{holdingpage_html} maintenance.html; fi"
        end

        desc "Creates a holding page"
        task :on, :roles => :web do
          run "cd #{current_path} && ln -sf #{holdingpage_html} maintenance.html"
        end

        desc "Removes all holding pages"
        task :off, :roles => :web do
          run "cd #{current_path} && rm -f maintenance.html"
          run "cd #{releases_path}/ && rm -f ./{*}/maintenance.html"
        end

        desc "Reminder to turn off the holding page"
        task :reminder do
          puts "\nREMEMBER TO TURN OFF THE HOLDING PAGE WHEN YOU HAVE FINISHED TESTING THE SITE\n\n"
        end
      end
    end
  end
end


if Capistrano::Configuration.instance
  CapeZHoldingpage.load_into(Capistrano::Configuration.instance)
end
